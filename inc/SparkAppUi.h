/*
 * WheelAppUi.h
 *
 *  Created on: Jul 10, 2009
 *      Author: dev
 */

#ifndef WHEELAPPUI_H_
#define WHEELAPPUI_H_

#include <aknviewappui.h>

class CSparkView;
class CSettingsView;

class CSparkAppUi : public CAknViewAppUi
	{
public:
    void ConstructL();
    ~CSparkAppUi();

private:
    // Inherirted from class CEikAppUi
    void HandleCommandL(TInt aCommand);

private:
	void CreateViewsL ();
    void SwitchToSettingsViewL ();
    void SwitchToSparkViewL ();
	
    CSparkView* iSparkView;
    CSettingsView* iSettingsView;
	};

#endif /* WHEELAPPUI_H_ */
