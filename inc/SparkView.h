/*
 * WheelView.h
 *
 *  Created on: Jul 10, 2009
 *      Author: dev
 */

#ifndef WHEELVIEW_H_
#define WHEELVIEW_H_

#include <e32cmn.h>
#include <aknview.h>

const TUid KSparkViewId = {1};

class CSparkControl;

class CSparkView : public CAknView
	{
public:
	static CSparkView* NewL();
	static CSparkView* NewLC();
	~CSparkView();
	
public:
	void HandleCommandL (TInt aCommand);
	
private:
	
	TUid Id () const;
    void DoActivateL(const TVwsViewId& aPrevViewId,TUid aCustomMessageId,
                       const TDesC8& aCustomMessage);
    void DoDeactivate();
    
private:
	CSparkView ();
	void ConstructL ();
	
private:
	CSparkControl* iContainer;
	};

#endif /* WHEELVIEW_H_ */
