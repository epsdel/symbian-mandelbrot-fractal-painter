/*
 * PixelIterator.h
 *
 *  Created on: Jul 16, 2009
 *      Author: Dan
 */

#ifndef ITERATOR_H_
#define ITERATOR_H_

#include "Complex.h"

class TIterator
	{
public:
	TIterator ();
	TIterator (const TComplex& aStart, const TComplex& aEnd);
	
	void Reset ();
	void Reset (const TComplex& aStart, const TComplex& aEnd);
	void Step ();
	TBool IsDone () const;
	
	//	returns the cursor
	//	if IsDone () returns true, the cursor will be invalid and should not be used  
	const TComplex& GetCursor () const;
	
private:
	TComplex iStart;
	TComplex iEnd;
	TComplex iCursor;
	
	TBool iValid;	//	used for internal management
	};

#endif /* PIXELITERATOR_H_ */
