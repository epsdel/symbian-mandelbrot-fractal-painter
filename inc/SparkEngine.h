/*
 * Engine.h
 *
 *  Created on: Jul 2, 2009
 *      Author: dev
 */

#ifndef ENGINE_H_
#define ENGINE_H_

#include "Toolkit.h"
#include <w32std.h>

#define SPARKENGINE_UPDATE_DELAY 30000
#define SPARKENGINE_OFFSCREEN_DISPLAYMODE EColor64K
#define SPARKENGINE_FRACTAL_COUNT 3

class CEikonEnv;
class CAknViewAppUi;
class RWindow;
class CWsScreenDevice;
class CDirectScreenAccess;
class CFbsBitGc;
class RRegion;
class CFbsBitmap;
class CBitmapContext;
class CFbsBitmapDevice;
class CFractalBase;

class CSparkEngine : public CBase, public MDirectScreenAccess
	{
public:
	CSparkEngine( CEikonEnv& aEnv, CAknViewAppUi* aAppUi );
	~CSparkEngine();
	void ConstructL( RWindow& aWindow, CWsScreenDevice* aScreenDevice );

private:
	static TInt Update(TAny* aObj);

	void Render ();
	
private:
	// Implement MDirectScreenAccess
	void Restart( RDirectScreenAccess::TTerminationReasons aReason );
	void AbortNow( RDirectScreenAccess::TTerminationReasons aReason );

public:	//	own methods
	void SetPaused( TBool aPause );
	TBool IsPaused() const;

	void PreviousFractal ();
	void NextFractal ();
	void PreviousFunction ();
	void NextFunction ();
	void ForceRedraw ();
	
	void OpenSettingsL ();
	
private:
	void ReconstructFractalL ();
	
	CEikonEnv& iEnv; //	BYREF

	CAknViewAppUi* iAppUi; // BYREF
	CWsScreenDevice* iScreenDevice; // BYREF

	// Periodic for drawing graphics
	CPeriodic* iAnimationTimer;

	// Direct Screen Access
	CDirectScreenAccess* iDirectScreenAccess; // OWNED
	CFbsBitGc* iGc; // BYREF, from DSA
	RRegion* iRegion; // BYREF, from DSA

	// Offscreen bitmap for double-buffering
	CFbsBitmap* iBackBuffer; // OWNED
	CBitmapContext* iBackContext; // OWNED
	CFbsBitmapDevice* iBackDevice; // OWNED

	TBool iPaused;
	
	TToolkit iToolKit;
	CFractalBase* iFractal;
	TInt iIndex;
	TBool iReconstruct;
	};

#endif /* ENGINE_H_ */
