/*
 * Fractal.h
 *
 *  Created on: Apr 10, 2009
 *      Author: dev
 */

#ifndef FRACTAL_H_
#define FRACTAL_H_

#include "Complex.h"
#include "Iterator.h"
#include "Toolkit.h"
#include <e32base.h>
#include <e32std.h>
#include <gdi.h>

class MFractalVisitor;

class CFractalBase : public CBase
	{
public:
	enum TFractalState { EInvalid, ESuspended, ENextTask, EComplete };
	
	void Invalidate ();	
	void Suspend ();
	void Restore ();
	CFractalBase::TFractalState GetState () const;
	
	virtual void Render (CGraphicsContext& aContext) = 0;
	virtual ~CFractalBase () = 0;
	
	virtual void AcceptVisitorL( MFractalVisitor& aVisitor ) const = 0;
	
protected:
	CFractalBase( const TToolkit& aToolkit );
	
	const TToolkit& iToolkit;	//	rendering toolkit, only const methods are accessible
	
	TFractalState iLastState;
	TFractalState iState;
	};


#endif /* FRACTAL_H_ */
