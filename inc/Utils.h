/*
 * Utils.h
 *
 *  Created on: Apr 10, 2009
 *      Author: dev
 */

#ifndef UTILS_H_
#define UTILS_H_

#include "Complex.h"
#include "Matrix2D.h"
#include <e32cmn.h>

inline TBool IsDigit( TInt8 aChar )
	{
	return ( aChar >= '0' && aChar <= '9' );
	}

TReal ComputeDistance (const TPoint& aPt1, const TPoint& aPt2);

TInt NormalizeDegrees (TInt degrees);

TReal32 DegreesToRadians (TInt degrees);

TPoint operator* (TReal32 aScalar, const TPoint& aPt);

TPoint operator* (const TPoint& aPt, TReal32 aScalar);

TComplex ConvertToComplex (const TPoint& aPt);

TPoint ConvertToPoint (const TComplex& aComp);

//	apply the transform aTransform on the planar point aComplex
//	aComplex becomes aTransform * aComplex 
void ApplyTransform (const TMatrix2D& aTransform, TComplex& aComplex);

//	utility template pair class
//		- use type traits for param passing, when available
template<class First, class Second>
class TPair
	{
public:
	First iFirst;
	Second iSecond;

	TPair()
		{
		}

	TPair( const First& aFirst, const Second& aSecond ) :
		iFirst( aFirst ), iSecond( aSecond )
		{
		}
	};

template<class First, class Second> TPair<First, Second> MakePair(
	const First& aFirst, const Second& aSecond );

#endif /* UTILS_H_ */
