/*
 * Mandelbrot.h
 *
 *  Created on: Jul 29, 2009
 *      Author: dev
 */

#ifndef MANDELBROT_H_
#define MANDELBROT_H_

#include "Toolkit.h"
#include "Fractal.h"

#include <e32def.h>
#include <e32cmn.h>

#define FRACTAL_LINE_COUNT_PER_TASK 16

typedef RArray<TRgb> TPalette;
typedef TComplex (*TIterativeFunction) (const TComplex& aZ, const TComplex& aC);

class CMandelbrot : public CFractalBase
	{
public:
	static CMandelbrot* NewL( const TToolkit& aToolkit, const TFileName& aPaletteFileName );
	static CMandelbrot* NewLC( const TToolkit& aToolkit, const TFileName& aPaletteFileName );
		
public:	//	from MFractal
	virtual ~CMandelbrot ();
	virtual void Render( CGraphicsContext& aContext );

public:	//	from MFractalVisitor
	virtual void AcceptVisitorL( MFractalVisitor& aVisitor ) const;
	
public:	
	void SetSeed( TComplex aSeed );
	TComplex GetSeed() const;

	void SetBailoutRadius( TReal aRadius );
	TReal GetBailoutRaidius() const;

	void SetMaxIterations( TInt aMax );
	TInt GetMaxIterations() const;

	void SetFunction( TIterativeFunction aFunction );
	TIterativeFunction GetFunction() const;

	TPalette& GetPalette();
	
private:
	CMandelbrot( const TToolkit& aToolkit );
	void ConstructL( const TFileName& aFileName );
	TBool LoadPaletteFromFileL( const TDesC& aPath, TPalette& aPalette );
	
	void RenderTask( CGraphicsContext& aContext );
	
	TPalette iPalette;
	
	//	Fractal information
	TIterativeFunction iFunction;
	TComplex iSeed;
	TReal iBailoutRadius;
	TInt iMaxIterations;
	
	TIterator iIterator;
	TUint iPixelsToPlotCount;
	TUint iPlottedPixelsCount;
	};

#endif /* MANDELBROT_H_ */
