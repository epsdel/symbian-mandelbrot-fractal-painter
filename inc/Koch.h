/*
 * Koch.h
 *
 *  Created on: Jul 29, 2009
 *      Author: dev
 */

#ifndef KOCH_H_
#define KOCH_H_

#include "Fractal.h"

#define KOCH_DEFAULT_MAX_RECURSION_DEPTH 5
#define KOCH_DEFAULT_DISTANCE_FROM_CENTER 1.5f

class CKoch : public CFractalBase
	{
public:
	static CKoch* NewL( const TToolkit& aToolkit );
	static CKoch* NewLC( const TToolkit& aToolkit );
	
public:	//	from MFractal
	virtual void Render( CGraphicsContext& aContext );
	virtual ~CKoch ();
	
public:	//	from MFractalVisitor
	virtual void AcceptVisitorL( MFractalVisitor& aVisitor ) const;
	
public:
	void SetMaxRecursionDepth (TInt aLevel);
	TInt GetMaxRecursionDepth () const;

	void SetDistanceFromCenter( TReal aDist );
	TReal GetDistanceFromCenter() const;
	
	void SetColor (TRgb aColor);
	TRgb GetColor () const;
	
	void SetBackColor (TRgb aColor);
	TRgb GetBackColor () const;
	
	void SetCenter (TComplex aCenter);
	TComplex GetCenter () const;
	
private:
	CKoch ( const TToolkit& aToolkit );
	void ConstructL();
	
	void ResetPoints();
	void RenderSnowflake( TInt aDepth, CGraphicsContext& aContext, const TComplex& aPt1, const TComplex& aPt2 );
	
	TInt iMaxRecursionDepth;
	TReal iDistanceFromCenter;
	TComplex iPoints[3];	
	TRgb iBackColor;
	TRgb iColor;
	TComplex iCenter;
	};

#endif /* KOCH_H_ */
