/*
 * CSettingsControl.h
 *
 *  Created on: Oct 7, 2009
 *      Author: dan.munteanu
 */

#ifndef SETTINGSCONTROL_H_
#define SETTINGSCONTROL_H_

#include <coecntrl.h>

class CAknViewAppUi;

class CSettingsControl : public CCoeControl, public MCoeControlObserver
	{
public:
	CSettingsControl ();
	
	void ConstructL( const TRect& aRect, CAknViewAppUi* aAppUi );
	~CSettingsControl();

public:
	void HandleEnterPressL();

	void SetIndex (TInt aIndex);
	
public:
	virtual void Draw(const TRect& aRect) const;
	
private:
	// From CoeControl,CountComponentControls.
	TInt CountComponentControls() const;

	// Event handling section
	void HandleControlEventL( CCoeControl* aControl, TCoeEvent aEventType );
	void HandleResourceChange( TInt aType );
	TKeyResponse OfferKeyEventL( const TKeyEvent& aKeyEvent, TEventCode aType );
	void SizeChanged();
	void HandlePointerEventL( const TPointerEvent& aPointerEvent );
	
	CAknViewAppUi* iAppUi;
	TInt iIndex;
	};

#endif /* CSETTINGSCONTROL_H_ */
