/*
 ============================================================================
 Name		: SparkDocument.h
 Author	  : Dan M
 Copyright   : Use at your own risk
 Description : Declares document class for application.
 ============================================================================
 */

#ifndef __SPARKDOCUMENT_h__
#define __SPARKDOCUMENT_h__

// INCLUDES
#include <akndoc.h>

// FORWARD DECLARATIONS
class CSparkAppUi;
class CEikApplication;
class CFractalBase;

// CLASS DECLARATION

/**
 * CSparkDocument application class.
 * An instance of class CSparkDocument is the Document part of the
 * AVKON application framework for the Spark example application.
 */
class CSparkDocument : public CAknDocument
	{
public:
	// Constructors and destructor

	/**
	 * NewL.
	 * Two-phased constructor.
	 * Construct a CSparkDocument for the AVKON application aApp
	 * using two phase construction, and return a pointer
	 * to the created object.
	 * @param aApp Application creating this document.
	 * @return A pointer to the created instance of CSparkDocument.
	 */
	static CSparkDocument* NewL( CEikApplication& aApp );

	/**
	 * NewLC.
	 * Two-phased constructor.
	 * Construct a CSparkDocument for the AVKON application aApp
	 * using two phase construction, and return a pointer
	 * to the created object.
	 * @param aApp Application creating this document.
	 * @return A pointer to the created instance of CSparkDocument.
	 */
	static CSparkDocument* NewLC( CEikApplication& aApp );

	/**
	 * ~CSparkDocument
	 * Virtual Destructor.
	 */
	virtual ~CSparkDocument();
	
	static CSparkDocument* Static();

	void SetActiveFractal (CFractalBase* aFractal);
	CFractalBase* GetActiveFractal () const;
	
public:
	// Functions from base classes

	/**
	 * CreateAppUiL
	 * From CEikDocument, CreateAppUiL.
	 * Create a CSparkAppUi object and return a pointer to it.
	 * The object returned is owned by the Uikon framework.
	 * @return Pointer to created instance of AppUi.
	 */
	CEikAppUi* CreateAppUiL();

private:
	// Constructors

	/**
	 * ConstructL
	 * 2nd phase constructor.
	 */
	void ConstructL();

	/**
	 * CSparkDocument.
	 * C++ default constructor.
	 * @param aApp Application creating this document.
	 */
	CSparkDocument( CEikApplication& aApp );

	CFractalBase* iFractal;	
	};

#endif // __SPARKDOCUMENT_h__
// End of File
