/*
 ============================================================================
 Name		: Spark.pan
 Author	  : Dan M
 Copyright   : Use at your own risk
 Description : This file contains panic codes.
 ============================================================================
 */

#ifndef __SPARK_PAN__
#define __SPARK_PAN__

/** Spark application panic codes */
enum TSparkPanics
	{
	ESparkUi = 1
	// add further panics here
	};

inline void Panic( TSparkPanics aReason )
	{
	_LIT( applicationName, "Spark" );
	User::Panic( applicationName, aReason );
	}

#endif // __SPARK_PAN__
