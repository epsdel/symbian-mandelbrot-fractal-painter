/*
 * IterativeFunctions.h
 *
 *  Created on: Aug 7, 2009
 *      Author: dev
 */

#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

#include "Complex.h"

TComplex ZSquarePlusC( const TComplex& aZ, const TComplex& aC );
TComplex ZCubePlusC( const TComplex& aZ, const TComplex& aC );
TComplex ZQuadPlusC( const TComplex& aZ, const TComplex& aC );
TComplex ZHexaPlusC( const TComplex& aZ, const TComplex& aC );
TComplex Custom ( const TComplex& aZ, const TComplex& aC );

#endif /* ITERATIVEFUNCTIONS_H_ */
