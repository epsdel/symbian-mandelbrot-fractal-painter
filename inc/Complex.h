/*
 * Complex.h
 *
 *  Created on: Apr 10, 2009
 *      Author: dev
 */

#ifndef COMPLEX_H_
#define COMPLEX_H_

#include <e32def.h>
#include <e32cmn.h>

class TMatrix2D;

class TComplex
	{
public:
	TComplex();
	TComplex( TReal aX, TReal aY );

	void SetXY( TReal aX, TReal aY );

	TComplex& operator*=( const TComplex& aComplex );
	TComplex& operator*=( TReal aScalar );
	TComplex& operator+=( const TComplex& aComplex );
	TComplex& operator-=( const TComplex& aComplex );
	
	TComplex operator+( const TComplex& aComplex );

	TBool operator==( const TComplex& aComplex ) const;
	TBool operator !=( const TComplex& aComplex ) const;

	TReal Abs() const;
	TComplex Sqr() const;

	TReal iX;
	TReal iY;
	};

TComplex operator* (const TComplex& aComplex, TReal aScalar);
TComplex operator* (TReal aScalar, const TComplex& aComplex);

#endif /* COMPLEX_H_ */
