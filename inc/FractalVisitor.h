/*
 * FractalVisitor.h
 *
 *  Created on: Sep 30, 2009
 *      Author: dan.munteanu
 */

#ifndef FRACTALVISITOR_H_
#define FRACTALVISITOR_H_

class CFractalBase;
class CMandelbrot;
class CKoch;
class CSierpinsky;

class MFractalVisitor
	{
public:
	virtual ~MFractalVisitor () = 0;
	
	virtual void VisitL( const CMandelbrot& aFractal ) = 0;
	virtual void VisitL( const CKoch& aFractal ) = 0;
	virtual void VisitL( const CSierpinsky& aFractal ) = 0;
	virtual void VisitL( const CFractalBase& aFractal ) = 0;
	};

#endif /* FRACTALVISITOR_H_ */
