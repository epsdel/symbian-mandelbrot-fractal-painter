/*
 * DTK.h
 *
 *  Created on: Jul 10, 2009
 *      Author: dev
 */

#ifndef _TOOLKIT_H
#define _TOOLKIT_H

#include "Utils.h"
#include "Matrix2D.h"
#include <e32std.h>

typedef TPair<TComplex, TComplex> TWindow;

struct TToolkit
	{
public:
	TToolkit();

	//	sets what region of the screen we will see the graphics in 
	//	causes a call to UpdateTransforms which is computationally expensive
	void SetScreenView( const TRect& aRect );

	const TRect& GetScreenView() const;

	//	sets what is the visible window into the 2D world
	//	causes a call to UpdateTransforms which is computationally expensive
	void SetWorldView( TComplex aTopLeft, TReal aWidth, TReal aHeight );
	void SetWorldView( TComplex aTopLeft, TComplex aBottomRight );

	const TWindow& GetWorldView() const;

	//	Setting the fit ratios flag will have effect only on the next SetWorldViewCall 
	void SetFitRatios( TBool aFitRatios );

	TBool IsFitRatios() const;

	//	Setting the scale width flag will have effect if the fit ratios flag is true and only on the next SetWorldViewCall
	void SetScaleWidth( TBool aScaleWidth );

	TBool IsScaleWidth() const;

	//	Setting the landscape flag will have effect only on the next SetWorldView call
	void SetLandscape( TBool aLandscape );

	TBool IsLandscape() const;

	//	Setting the mirror flag will have effect only on the next SetWorldView call
	void SetMirrored( TBool aMirrored );

	TBool IsMirrored() const;

	const TMatrix2D& GetWorldToScreen() const;
	const TMatrix2D& GetScreenToWorld() const;
	
private:
	void FitRatios ();
	
	//	UpdateTransforms performs a (re)computing of iScreenToWorld and iWorldToScreen matrices
	//	It is computationally expensive, so an indirect call (check the commentaries in the public methods) should be caused as seldom as possible
	//	Though before any computations, it checks the widths and heights of the screen and world views, respectively
	// 	If any of these are equal to zero, no computations are performed, since they would be useless
	void UpdateTransforms ();
	
	TWindow iWorldView;
	TRect iScreenView;
	TMatrix2D iScreenToWorld;
	TMatrix2D iWorldToScreen;
	TBool iFitRatios;	//	do we want to fit world ratio to screen ratio?
	TBool iScaleWidth;	//	ETrue if we modify world width, EFalse if world height (only when iFitRatios is ETrue)
	TBool iLandscape;
	TBool iMirrored;
	};

#endif
