/*
 * Sierpinsky.h
 *
 *  Created on: Aug 21, 2009
 *      Author: dev
 */

#ifndef SIERPINSKY_H_
#define SIERPINSKY_H_

#include "Fractal.h"

#define SIERPINSKY_MAX_RECURSION_DEPTH 5

class CSierpinsky : public CFractalBase
	{
public:
	static CSierpinsky* NewL( const TToolkit& aToolkit );
	static CSierpinsky* NewLC( const TToolkit& aToolkit );
	
public:	//	from MFractal
	virtual void Render( CGraphicsContext& aContext );
	virtual ~CSierpinsky ();
	
public:	//	from MFractalVisitor
	virtual void AcceptVisitorL( MFractalVisitor& aVisitor ) const;
	
private:
	CSierpinsky ( const TToolkit& aToolkit );
	void ConstructL();

	void ResetPoints();
	void RenderTriangle( TInt aDepth, CGraphicsContext& aContext, const TComplex* aPts );
	
	TComplex iCenter;
	TReal iDistanceFromCenter;
	TComplex iPoints[3];
	TRgb iBackColor;
	TRgb iColor;	
	};

#endif /* SIERPINSKY_H_ */
