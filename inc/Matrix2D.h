/*
 * TMatrix2.h
 *
 *  Created on: Apr 7, 2009
 *      Author: dev
 */

#ifndef MATRIX2_H_
#define MATRIX2_H_

#include "Complex.h"
#include <e32cmn.h>

class TMatrix2D
	{
public:
	TMatrix2D ();
	
	void SetAt (TInt iIdx, TInt iJdx, TReal32 val);
	TReal32 GetAt (TInt iIdx, TInt iJdx) const;
	
	TMatrix2D& operator+= (const TMatrix2D& aMatrix);
	TMatrix2D& operator-= (const TMatrix2D& aMatrix);
	TMatrix2D& operator*= (const TMatrix2D& aMatrix);
	TMatrix2D& operator*= (TReal32 aScalar);

	void Transpose ();
	
private:
	TReal32 iVal[3][3];
	};

void LoadIdentity( TMatrix2D& aMatrix );
void LoadZero( TMatrix2D& aMatrix );
void LoadRotation( TMatrix2D& aMatrix, TInt aDegrees );
void LoadTranslation( TMatrix2D& aMatrix, TReal32 aX, TReal32 aY );
void LoadScale( TMatrix2D& aMatrix, TReal32 aX, TReal32 aY );
void LoadMirror( TMatrix2D& aMatrix, TChar aXorY );

void ComputeDeterminant( const TMatrix2D& aMatrix, TReal& aResult );
void ComputeInverse( const TMatrix2D& aMatrix, TMatrix2D& aInverse );
void ComputeTransposed( const TMatrix2D& aMatrix, TMatrix2D& aTransposed );

#endif /* TMATRIX2_H_ */
