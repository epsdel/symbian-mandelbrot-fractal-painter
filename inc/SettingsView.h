/*
 * SettingsView.h
 *
 *  Created on: Sep 30, 2009
 *      Author: dan.munteanu
 */

#ifndef SETTINGSVIEW_H_
#define SETTINGSVIEW_H_

#include <aknview.h>

#include "FractalVisitor.h"

const TUid KSparkSettingsViewId = {2};

class CFractalBase;
class CMandelbrot;
class CKoch;
class CSierpinsky;
class CSettingsControl;

class CSettingsView : public CAknView, private MFractalVisitor
	{
public:
	static CSettingsView* NewL ();
	static CSettingsView* NewLC ();	
	
	virtual ~CSettingsView ();

public:	//	from CAknView	
	TUid Id() const;
	void DoActivateL( const TVwsViewId& aPrevViewId, TUid aCustomMessageId,
		const TDesC8& aCustomMessage );
	void DoDeactivate();
	
private:
	CSettingsView ();
	void ConstructL ();
	
	void VisitL( const CMandelbrot& aFractal );
	void VisitL( const CKoch& aFractal );
	void VisitL( const CSierpinsky& aFractal );
	void VisitL( const CFractalBase& aFractal );
	
	CSettingsControl* iControl;
	};

#endif /* SETTINGSVIEW_H_ */
