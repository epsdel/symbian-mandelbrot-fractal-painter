/*
 ============================================================================
 Name		: SparkApplication.h
 Author	  : Dan M
 Copyright   : Use at your own risk
 Description : Declares main application class.
 ============================================================================
 */

#ifndef __SPARKAPPLICATION_H__
#define __SPARKAPPLICATION_H__

// INCLUDES
#include <aknapp.h>
#include "Spark.hrh"

// UID for the application;
// this should correspond to the uid defined in the mmp file
const TUid KUidSparkApp =
	{
	_UID3
	};

// CLASS DECLARATION

/**
 * CSparkApplication application class.
 * Provides factory to create concrete document object.
 * An instance of CSparkApplication is the application part of the
 * AVKON application framework for the Spark example application.
 */
class CSparkApplication : public CAknApplication
	{
public:
	// Functions from base classes

	/**
	 * From CApaApplication, AppDllUid.
	 * @return Application's UID (KUidSparkApp).
	 */
	TUid AppDllUid() const;

protected:
	// Functions from base classes

	/**
	 * From CApaApplication, CreateDocumentL.
	 * Creates CSparkDocument document object. The returned
	 * pointer in not owned by the CSparkApplication object.
	 * @return A pointer to the created document object.
	 */
	CApaDocument* CreateDocumentL();
	};

#endif // __SPARKAPPLICATION_H__
// End of File
