#ifndef SPARKCONTROL_H_
#define SPARKCONTROL_H_

#include <coecntrl.h>

class CAknViewAppUi;
class CSparkEngine;

class CSparkControl : public CCoeControl, public MCoeControlObserver
	{
public:
	// Symbian default constructor.
	void ConstructL( const TRect& aRect, CAknViewAppUi* aAppUi );
	~CSparkControl();

private:
	// Functions from base classes

	// From CoeControl,CountComponentControls.
	TInt CountComponentControls() const;

	// Event handling section
	void HandleControlEventL( CCoeControl* aControl, TCoeEvent aEventType );
	void HandleResourceChange( TInt aType );
	TKeyResponse OfferKeyEventL( const TKeyEvent &aKeyEvent, TEventCode aType );
	void SizeChanged();
	void HandlePointerEventL( const TPointerEvent& aPointerEvent );

protected:
	CSparkEngine* iEngine;	
	};

#endif /* WHEELCONTROL_H_ */
