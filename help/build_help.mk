# ============================================================================
#  Name	 : build_help.mk
#  Part of  : Spark
# ============================================================================
#  Name	 : build_help.mk
#  Part of  : Spark
#
#  Description: This make file will build the application help file (.hlp)
# 
# ============================================================================

do_nothing :
	@rem do_nothing

# build the help from the MAKMAKE step so the header file generated
# will be found by cpp.exe when calculating the dependency information
# in the mmp makefiles.

MAKMAKE : Spark_0xE7D8F8E3.hlp
Spark_0xE7D8F8E3.hlp : Spark.xml Spark.cshlp Custom.xml
	cshlpcmp Spark.cshlp
ifeq (WINS,$(findstring WINS, $(PLATFORM)))
	copy Spark_0xE7D8F8E3.hlp $(EPOCROOT)epoc32\$(PLATFORM)\c\resource\help
endif

BLD : do_nothing

CLEAN :
	del Spark_0xE7D8F8E3.hlp
	del Spark_0xE7D8F8E3.hlp.hrh

LIB : do_nothing

CLEANLIB : do_nothing

RESOURCE : do_nothing
		
FREEZE : do_nothing

SAVESPACE : do_nothing

RELEASABLES :
	@echo Spark_0xE7D8F8E3.hlp

FINAL : do_nothing
