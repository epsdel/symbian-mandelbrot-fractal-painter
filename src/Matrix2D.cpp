#include "Matrix2D.h"
#include <e32std.h>
#include <e32math.h>

TMatrix2D::TMatrix2D ()
	{
	//	load zero values
	memset(iVal, 0, sizeof(TReal32) * 9);
	}
	
void TMatrix2D::SetAt (TInt iIdx, TInt iJdx, TReal32 val)
	{
	if (!(iIdx >= 0 && iIdx < 3) && !(iJdx >= 0 && iJdx < 3))
		return;
	
	iVal[iIdx][iJdx] = val;
	}

TReal32 TMatrix2D::GetAt (TInt iIdx, TInt iJdx) const
{
	if ((iIdx >= 0 && iIdx < 3) && (iJdx >= 0 && iJdx < 3))
		return iVal[iIdx][iJdx]; 
}

TMatrix2D& TMatrix2D::operator+= (const TMatrix2D& aMatrix)
	{
	for ( TInt idx = 0; idx < 3; ++idx )
		{
		for ( TInt jdx = 0; jdx < 3; ++jdx )
			{
			iVal[idx][jdx] += aMatrix.iVal[idx][jdx];
			}
		}		
	return *this;
	}

TMatrix2D& TMatrix2D::operator-= (const TMatrix2D& aMatrix)
	{
	for ( TInt idx = 0; idx < 3; ++idx )
		{
		for ( TInt jdx = 0; jdx < 3; ++jdx )
			{
			iVal[idx][jdx] -= aMatrix.iVal[idx][jdx];
			}
		}
	return *this;
	}

TMatrix2D& TMatrix2D::operator*= (const TMatrix2D& aMatrix)
	{
	TReal32 result[3][3];
	for ( TInt idx = 0; idx < 3; ++idx )
		for ( TInt jdx = 0; jdx < 3; ++jdx )
			{
			TReal32 sum = 0;
			for ( TInt kdx = 0; kdx < 3; ++kdx )
				{
				sum += iVal[idx][kdx] * aMatrix.iVal[kdx][jdx];
				}
			result[idx][jdx] = sum;
			}
	memcpy( iVal, result, sizeof(TReal32) * 9 );
	return *this;
	}

TMatrix2D& TMatrix2D::operator*= (TReal32 aScalar)
	{
	for ( TInt idx = 0; idx < 3; ++idx )
		{
		for ( TInt jdx = 0; jdx < 3; ++jdx )
			{
			iVal[idx][jdx] *= aScalar;
			}
		}
	return *this;
	}

void TMatrix2D::Transpose ()
	{
	TReal temp = iVal[0][1];
	iVal[0][1] = iVal[1][0];
	iVal[1][0] = temp;
	
	temp = iVal[2][1];
	iVal[2][1] = iVal[1][2];
	iVal[1][2] = temp;

	temp = iVal[2][0];
	iVal[2][0] = iVal[0][2];
	iVal[0][2] = temp;	
	}

void LoadIdentity (TMatrix2D& aMatrix)
	{
	aMatrix.SetAt( 0, 0, 1 );
	aMatrix.SetAt( 0, 1, 0 );
	aMatrix.SetAt( 0, 2, 0 );
	aMatrix.SetAt( 1, 0, 0 );
	aMatrix.SetAt( 1, 1, 1 );
	aMatrix.SetAt( 1, 2, 0 );
	aMatrix.SetAt( 2, 0, 0 );
	aMatrix.SetAt( 2, 1, 0 );
	aMatrix.SetAt( 2, 2, 1 );
	}

void LoadZero (TMatrix2D& aMatrix)
	{
	aMatrix.SetAt( 0, 0, 0 );
	aMatrix.SetAt( 0, 1, 0 );
	aMatrix.SetAt( 0, 2, 0 );
	aMatrix.SetAt( 1, 0, 0 );
	aMatrix.SetAt( 1, 1, 0 );
	aMatrix.SetAt( 1, 2, 0 );
	aMatrix.SetAt( 2, 0, 0 );
	aMatrix.SetAt( 2, 1, 0 );
	aMatrix.SetAt( 2, 2, 0 );
	}

void LoadRotation (TMatrix2D& aMatrix, TInt aDegrees)
	{
	TReal result;
	TReal32 radians = (KPi / 180) * aDegrees;
	
	//	set the rotation values
	Math::Cos (result, radians);	
	aMatrix.SetAt (0, 0, result);
	aMatrix.SetAt (1, 1, result);
	aMatrix.SetAt (2, 2, 1);
	Math::Sin (result, radians);
	aMatrix.SetAt (0, 1, -result);
	aMatrix.SetAt (1, 0, result);

	//	set the zeros
	aMatrix.SetAt (0, 2, 0);
	aMatrix.SetAt (1, 2, 0);
	aMatrix.SetAt (2, 0, 0);
	aMatrix.SetAt (2, 1, 0);
	}

void LoadTranslation (TMatrix2D& aMatrix, TReal32 aTransX, TReal32 aTransY)
	{
	aMatrix.SetAt( 0, 0, 1 );
	aMatrix.SetAt( 0, 1, 0 );
	aMatrix.SetAt( 0, 2, aTransX );
	aMatrix.SetAt( 1, 0, 0 );
	aMatrix.SetAt( 1, 1, 1 );
	aMatrix.SetAt( 1, 2, aTransY );
	aMatrix.SetAt( 2, 0, 0 );
	aMatrix.SetAt( 2, 1, 0 );
	aMatrix.SetAt( 2, 2, 1 );
	}

void LoadScale (TMatrix2D& aMatrix, TReal32 aScaleX, TReal32 aScaleY)
	{
	aMatrix.SetAt( 0, 0, aScaleX );
	aMatrix.SetAt( 0, 1, 0 );
	aMatrix.SetAt( 0, 2, 0 );
	aMatrix.SetAt( 1, 0, 0 );
	aMatrix.SetAt( 1, 1, aScaleY );
	aMatrix.SetAt( 1, 2, 0 );
	aMatrix.SetAt( 2, 0, 0 );
	aMatrix.SetAt( 2, 1, 0 );
	aMatrix.SetAt( 2, 2, 1 );
	}

void LoadMirror (TMatrix2D& aMatrix, TChar aXorY)
	{
	if ( aXorY == 'x' || aXorY == 'X' )
		{
		aMatrix.SetAt( 0, 0, -1 );
		aMatrix.SetAt( 0, 1, 0 );
		aMatrix.SetAt( 0, 2, 0 );
		aMatrix.SetAt( 1, 0, 0 );
		aMatrix.SetAt( 1, 1, 1 );
		aMatrix.SetAt( 1, 2, 0 );
		aMatrix.SetAt( 2, 0, 0 );
		aMatrix.SetAt( 2, 1, 0 );
		aMatrix.SetAt( 2, 2, 1 );
		}
	else if ( aXorY == 'y' || aXorY == 'Y' )
		{
		aMatrix.SetAt( 0, 0, 1 );
		aMatrix.SetAt( 0, 1, 0 );
		aMatrix.SetAt( 0, 2, 0 );
		aMatrix.SetAt( 1, 0, 0 );
		aMatrix.SetAt( 1, 1, -1 );
		aMatrix.SetAt( 1, 2, 0 );
		aMatrix.SetAt( 2, 0, 0 );
		aMatrix.SetAt( 2, 1, 0 );
		aMatrix.SetAt( 2, 2, 1 );
		}
	}

void ComputeDeterminant (const TMatrix2D& aMatrix, TReal& aResult)
	{
	aResult = aMatrix.GetAt( 0, 0 ) * aMatrix.GetAt( 1, 1 ) * aMatrix.GetAt( 2, 2 );
	aResult += aMatrix.GetAt( 0, 1 ) * aMatrix.GetAt( 1, 2 ) * aMatrix.GetAt( 2, 0 );
	aResult += aMatrix.GetAt( 0, 2 ) * aMatrix.GetAt( 1, 0 ) * aMatrix.GetAt( 2, 1 );
	aResult -= aMatrix.GetAt( 0, 2 ) * aMatrix.GetAt( 1, 1 ) * aMatrix.GetAt( 2, 0 );
	aResult -= aMatrix.GetAt( 0, 0 ) * aMatrix.GetAt( 1, 2 ) * aMatrix.GetAt( 2, 1 );
	aResult -= aMatrix.GetAt( 0, 1 ) * aMatrix.GetAt( 1, 0 ) * aMatrix.GetAt( 2, 2 );
	}

void ComputeInverse (const TMatrix2D& aMatrix, TMatrix2D& aResult)
	{
	//	From 3D Math Primer for Graphics and Game Development -> Inverse of a matrix
	
	TReal determinant = 0;
	ComputeDeterminant( aMatrix, determinant );
	
	if ( determinant != 0 )
		{
		//	compute the cofactors matrix
		
		TReal result = 0;
		
		//	(0, 0) a11 a22 - a12 a21
		result = aMatrix.GetAt( 1, 1 ) * aMatrix.GetAt( 2, 2 ) - aMatrix.GetAt( 1, 2 ) * aMatrix.GetAt( 2, 1 );
		aResult.SetAt( 0, 0, result );

		//	(0, 1) -a10 a22 + a12 a20
		result = - aMatrix.GetAt( 1, 0 ) * aMatrix.GetAt( 2, 2 ) + aMatrix.GetAt( 1, 2 ) * aMatrix.GetAt( 2, 0 );
		aResult.SetAt( 0, 1, result );

		//	(0, 2) a10 a21 - a11 a20
		result = aMatrix.GetAt( 1, 0 ) * aMatrix.GetAt( 2, 1 ) - aMatrix.GetAt( 1, 1 ) * aMatrix.GetAt( 2, 0 );
		aResult.SetAt( 0, 2, result );

		//	(1, 0) -a01 a22 + a02 a21
		result = - aMatrix.GetAt( 0, 1 ) * aMatrix.GetAt( 2, 2 ) + aMatrix.GetAt( 0, 2 ) * aMatrix.GetAt( 2, 1 );
		aResult.SetAt( 1, 0, result );

		//	(1, 1) a00 a22 - a02 a20
		result = aMatrix.GetAt( 0, 0 ) * aMatrix.GetAt( 2, 2 ) - aMatrix.GetAt( 0, 2 ) * aMatrix.GetAt( 2, 0 );
		aResult.SetAt( 1, 1, result );

		//	(1, 2) -a00 a21 + a01 a20
		result = - aMatrix.GetAt( 0, 0 ) * aMatrix.GetAt( 2, 1 ) + aMatrix.GetAt( 0, 1 ) * aMatrix.GetAt( 2, 0 );
		aResult.SetAt( 1, 2, result );

		//	(2, 0) a01 a12 - a02 a11
		result = aMatrix.GetAt( 0, 1 ) * aMatrix.GetAt( 1, 2 ) - aMatrix.GetAt( 0, 2 ) * aMatrix.GetAt( 1, 1 );
		aResult.SetAt( 2, 0, result );

		//	(2, 1) -a00 a12 + a02 a10
		result = - aMatrix.GetAt( 0, 0 ) * aMatrix.GetAt( 1, 2 ) + aMatrix.GetAt( 0, 2 ) * aMatrix.GetAt( 1, 0 );
		aResult.SetAt( 2, 1, result );

		//	(2, 2) a00 a11 - a01 a10
		result = aMatrix.GetAt( 0, 0 ) * aMatrix.GetAt( 1, 1 ) - aMatrix.GetAt( 0, 1 ) * aMatrix.GetAt( 1, 0 );
		aResult.SetAt( 2, 2, result );
		
		//	transpose the cofactors matrix
		aResult.Transpose();
		
		//	scale by inverted determinant
		aResult *= 1.0f / determinant;
		}	
	}

