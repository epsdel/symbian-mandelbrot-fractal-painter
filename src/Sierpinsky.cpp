/*
 * Sierpinsky.cpp
 *
 *  Created on: Aug 21, 2009
 *      Author: dev
 */

#include "Sierpinsky.h"
#include "FractalVisitor.h"

CSierpinsky* CSierpinsky::NewLC( const TToolkit& aToolkit )
	{
	CSierpinsky* me = new ( ELeave ) CSierpinsky( aToolkit );
	CleanupStack::PushL( me );
	me->ConstructL();
	return me;
	}

CSierpinsky* CSierpinsky::NewL( const TToolkit& aToolkit )
	{
	CSierpinsky* me = CSierpinsky::NewLC( aToolkit );
	CleanupStack::Pop( me );
	return me;
	}

void CSierpinsky::Render( CGraphicsContext& aContext )
	{
	switch ( iState )
		{
		case ENextTask:
			{
			RenderTriangle( 0, aContext, iPoints );
			iState = EComplete;
			}
			break;

		case ESuspended:
			//	rendering was suspended, do nothing
			break;

		case EComplete:
			//	we're done
			break;

		case EInvalid:
		default:
			{
			//	(re)set to initial state
			ResetPoints();
			iState = ENextTask;
			break;
			}
		}
	}

CSierpinsky::CSierpinsky( const TToolkit& aToolkit ) :
	CFractalBase( aToolkit )
	{
	}

void CSierpinsky::ConstructL()
	{
	iBackColor = KRgbBlack;
	iColor = KRgbRed;
	iCenter.SetXY( 0, 0 );
	iDistanceFromCenter = 2.0f;
	}

CSierpinsky::~CSierpinsky()
	{
	}

void CSierpinsky::ResetPoints()
	{
	TMatrix2D transform;
	TMatrix2D result;
		
	iPoints[0].SetXY( 0, iDistanceFromCenter );

	//	iPoints[1] = translate (center) * rotate (120) * translate (-center) * iPoints[0];
	LoadTranslation( transform, iCenter.iX, iCenter.iY );
	LoadRotation( result, 120 );
	transform *= result;
	LoadTranslation(result, -iCenter.iX, -iCenter.iY);
	transform *= result;
	iPoints[1].SetXY( 0, iDistanceFromCenter );
	ApplyTransform( transform, iPoints[1] );
	
	//	iPoints[2] = translate (center) * rotate (-120) * translate (-center) * iPoints[0];
	LoadTranslation( transform, iCenter.iX, iCenter.iY );
	LoadRotation( result, -120 );
	transform *= result;
	LoadTranslation( result, -iCenter.iX, -iCenter.iY );
	transform *= result;
	iPoints[2].SetXY( 0, iDistanceFromCenter );
	ApplyTransform( transform, iPoints[2] );
	}

void CSierpinsky::RenderTriangle( TInt aDepth, CGraphicsContext& aContext, const TComplex* aPts )
	{	
	if (aDepth >= SIERPINSKY_MAX_RECURSION_DEPTH) 
		return;

	TComplex resultPoints[3] = { aPts[0], aPts[1], aPts[2] };
	ApplyTransform( iToolkit.GetWorldToScreen(), resultPoints[0] );
	ApplyTransform( iToolkit.GetWorldToScreen(), resultPoints[1] );
	ApplyTransform( iToolkit.GetWorldToScreen(), resultPoints[2] );
	
	//	compute the corresponding pixels
	TPoint pixels[3] =
		{
			ConvertToPoint( resultPoints[0] ),
			ConvertToPoint( resultPoints[1] ),
			ConvertToPoint( resultPoints[2] )
		};	
	
	//	draw big triangle in red
	aContext.SetBrushColor( iColor );
	aContext.SetBrushStyle( CGraphicsContext::ESolidBrush );
	aContext.SetPenStyle( CGraphicsContext::ENullPen );
	aContext.DrawPolygon( pixels, 3 );
	
	//	compute inner triangle coords
	TReal32 t = 0.5f;

	TComplex innerPoints[3] =
		{
			( 1 - t ) * aPts[0] + t * aPts[1],
			( 1 - t ) * aPts[0] + t * aPts[2],
			( 1 - t ) * aPts[1] + t * aPts[2]
		};

	//	compute pixel coordinates
	resultPoints[0] = innerPoints[0];
	resultPoints[1] = innerPoints[1];
	resultPoints[2] = innerPoints[2];
	ApplyTransform( iToolkit.GetWorldToScreen(), resultPoints[0] );
	ApplyTransform( iToolkit.GetWorldToScreen(), resultPoints[1] );
	ApplyTransform( iToolkit.GetWorldToScreen(), resultPoints[2] );	
	pixels[0] = ConvertToPoint( resultPoints[0] );
	pixels[1] = ConvertToPoint( resultPoints[1] );
	pixels[2] = ConvertToPoint( resultPoints[2] );
	
	//	draw inner triangle in background color
	aContext.SetBrushColor( iBackColor );
	aContext.DrawPolygon( pixels, 3 );
		
	//	upper triangle
	resultPoints[0] = aPts[0];
	resultPoints[1] = innerPoints[0];
	resultPoints[2] = innerPoints[1];
	RenderTriangle( aDepth + 1, aContext, resultPoints );

	//	left triangle
	resultPoints[0] = innerPoints[0];
	resultPoints[1] = aPts[1];
	resultPoints[2] = innerPoints[2];
	RenderTriangle( aDepth + 1, aContext, resultPoints );

	//	right triangle
	resultPoints[0] = innerPoints[1];
	resultPoints[1] = innerPoints[2];
	resultPoints[2] = aPts[2];
	RenderTriangle( aDepth + 1, aContext, resultPoints );
	}

void CSierpinsky::AcceptVisitorL( MFractalVisitor& aVisitor ) const
	{
	aVisitor.VisitL( *this );
	}
