/*
 ============================================================================
 Name		: SparkApplication.cpp
 Author	  : Dan M
 Copyright   : Use at your own risk
 Description : Main application class
 ============================================================================
 */

// INCLUDE FILES
#include "Spark.hrh"
#include "SparkDocument.h"
#include "SparkApplication.h"

// ============================ MEMBER FUNCTIONS ===============================

// -----------------------------------------------------------------------------
// CSparkApplication::CreateDocumentL()
// Creates CApaDocument object
// -----------------------------------------------------------------------------
//
CApaDocument* CSparkApplication::CreateDocumentL()
	{
	// Create an Spark document, and return a pointer to it
	return CSparkDocument::NewL( *this );
	}

// -----------------------------------------------------------------------------
// CSparkApplication::AppDllUid()
// Returns application UID
// -----------------------------------------------------------------------------
//
TUid CSparkApplication::AppDllUid() const
	{
	// Return the UID for the Spark application
	return KUidSparkApp;
	}

// End of File
