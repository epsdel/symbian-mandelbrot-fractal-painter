/*
 * Utils.cpp
 *
 *  Created on: Apr 10, 2009
 *      Author: dev
 */

#include "Utils.h"
#include <e32math.h>

TReal ComputeDistance (const TPoint& aPt1, const TPoint& aPt2)
	{
	TReal sqrtRes;
	Math::Sqrt(sqrtRes, (aPt1.iX - aPt2.iX) * (aPt1.iX - aPt2.iX) + (aPt1.iY - aPt2.iY) * (aPt1.iY - aPt2.iY));
	return sqrtRes;
	}

TInt NormalizeDegrees (TInt degrees)
	{
	return degrees % 360;
	}

TReal32 DegreesToRadians (TInt degrees)
	{
	const TReal32 KPiOver180 = KPi / (TInt) 180;
	return degrees * KPiOver180;
	}

TPoint operator* (TReal32 aScalar, const TPoint& aPt)
	{
	return TPoint( static_cast<TInt> ( aScalar * aPt.iX ), static_cast<TInt> ( aScalar * aPt.iY ) );
	}

TPoint operator* (const TPoint& aPt, TReal32 aScalar)
	{
	return operator* (aScalar, aPt);
	}

TComplex ConvertToComplex (const TPoint& aPt)
	{
	return TComplex (aPt.iX, aPt.iY);
	}

TPoint ConvertToPoint (const TComplex& aComp)
	{
	return TPoint (aComp.iX, aComp.iY);
	}


void ApplyTransform (const TMatrix2D& aTransform, TComplex& aComplex)
	{
	TReal32 x = aTransform.GetAt (0, 0) * aComplex.iX + aTransform.GetAt (0, 1) * aComplex.iY + aTransform.GetAt (0, 2) * 1; 
	TReal32 y = aTransform.GetAt (1, 0) * aComplex.iX + aTransform.GetAt (1, 1) * aComplex.iY + aTransform.GetAt (1, 2) * 1;
	TReal32 w = aTransform.GetAt (2, 0) * aComplex.iX + aTransform.GetAt (2, 1) * aComplex.iY + aTransform.GetAt (2, 2) * 1;
	
	if (w != 0)
		{
		//	scale by 1 / w
		x /= w;
		y /= w;
		}
	
	//	update values
	aComplex.iX = x;
	aComplex.iY = y;
	}

template <class First, class Second> 
TPair<First, Second> MakePair (const First& aFirst, const Second& aSecond)
	{
	return TPair<First, Second> (aFirst, aSecond);
	}
