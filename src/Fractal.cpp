/*
 * Fractal.cpp
 *
 *  Created on: Jul 15, 2009
 *      Author: dev
 */

#include "Fractal.h"
#include <e32math.h>

CFractalBase::CFractalBase( const TToolkit& aToolkit ) :
	iToolkit (aToolkit),
	iLastState (EInvalid),
	iState (EInvalid)
	{	
	}

CFractalBase::~CFractalBase ()
	{	
	}

void CFractalBase::Invalidate()
	{
	iState = EInvalid;
	}

void CFractalBase::Suspend()
	{
	if (ESuspended != iState)
		{
		iLastState = iState;
		iState = ESuspended;
		}
	}

void CFractalBase::Restore()
	{
	if (ESuspended == iState)
		{
		iState = iLastState;
		}
	}

CFractalBase::TFractalState CFractalBase::GetState() const
	{
	return iState;
	}
