/*
 ============================================================================
 Name		: Spark.cpp
 Author	  : Dan M
 Copyright   : Use at your own risk
 Description : Main application class
 ============================================================================
 */

// INCLUDE FILES
#include <eikstart.h>
#include "SparkApplication.h"

LOCAL_C CApaApplication* NewApplication()
	{
	return new CSparkApplication;
	}

GLDEF_C TInt E32Main()
	{
	return EikStart::RunApplication( NewApplication );
	}

