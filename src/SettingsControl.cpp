#include "SettingsControl.h"
#include "Spark.hrh"

#include <akndef.h>
#include <aknviewappui.h>

CSettingsControl::CSettingsControl ()
	{	
	}

void CSettingsControl::SetIndex( TInt aIndex )
	{
	iIndex = aIndex;
	}

void CSettingsControl::ConstructL( const TRect& aRect, CAknViewAppUi* aAppUi )
	{
	iIndex = -1;
	iAppUi = aAppUi;
	
	CreateWindowL();
	SetExtentToWholeScreen();
	ActivateL();
	}

CSettingsControl::~CSettingsControl()
	{
	}

void CSettingsControl::HandleEnterPressL()
	{
	}

TInt CSettingsControl::CountComponentControls() const
	{
	return 0;
	}

void CSettingsControl::HandleControlEventL( CCoeControl* aControl,
	TCoeEvent aEventType )
	{

	}

void CSettingsControl::HandleResourceChange( TInt aType )
	{
	CCoeControl::HandleResourceChange( aType );
	if ( aType == KEikDynamicLayoutVariantSwitch )
		{
		SetExtentToWholeScreen();
		}
	}

TKeyResponse CSettingsControl::OfferKeyEventL( const TKeyEvent& aKeyEvent,
	TEventCode aType )
	{

	  if ( aType == EEventKey )
		{
		switch ( aKeyEvent.iScanCode )
			{
			case EStdKeyHash:
				iAppUi->HandleCommandL( ECommandOpenMainView );
				return EKeyWasConsumed;
			}
		}
	  				
	return EKeyWasNotConsumed;
	}

void CSettingsControl::SizeChanged()
	{
	}

void CSettingsControl::HandlePointerEventL( const TPointerEvent& aPointerEvent )
	{
	}

void CSettingsControl::Draw( const TRect& aRect ) const
	{
	CGraphicsContext* context = &SystemGc();
	
	

	context->SetBrushColor( KRgbBlack);
	context->SetBrushStyle( CGraphicsContext::ESolidBrush );
	context->DrawRect( aRect );
	
	context->SetPenColor( KRgbWhite);
	context->SetPenStyle( CGraphicsContext::ESolidPen );
	context->SetBrushColor( KRgbRed);
	
	switch (iIndex)
		{
		case 0:	//	mandelbrot -> circle
			{
			TRect central( TPoint( 120 - 40, 160 - 40 ), TSize( 80, 80 ) ); 
			context->DrawEllipse( central );
			}
			break;
			
		case 1:	//	koch -> rectangle
			{
			TRect central( TPoint( 120 - 40, 160 - 40 ), TSize( 80, 80 ) );
			context->DrawRoundRect( central, TSize( 25, 25 ) );
			}
			break;
			
		case 2:	//	sierpinsky -> triangle
			{
			TPoint center( 120, 160 );
			TPoint points[3] =
				{
				TPoint (center.iX - 50, center.iY),
				TPoint (center.iX + 50, center.iY),
				TPoint (center.iX, center.iY - 50)
				};
			context->DrawPolygon( points, 3 );
			}
			break;
			
		default:	//	bad index 
			{
			context->SetPenColor( KRgbYellow );
			context->DrawLine( TPoint( 120 - 25, 160 - 25 ), TPoint( 120 + 25,
				160 + 25 ) );
			}
			break;
		}
	}
