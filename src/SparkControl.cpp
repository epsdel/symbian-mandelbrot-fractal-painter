#include "SparkControl.h"
#include "SparkEngine.h"

#include <aknviewappui.h> 

// Second-phase constructor creates the engine object.
void CSparkControl::ConstructL( const TRect& /*aRect*/, CAknViewAppUi* aAppUi )
	{
	CreateWindowL();
	SetExtentToWholeScreen();

	iEngine = new ( ELeave ) CSparkEngine( *iEikonEnv, aAppUi );
	iEngine->ConstructL( Window(), CCoeEnv::Static()->ScreenDevice() );

	ActivateL();
	}

CSparkControl::~CSparkControl()
	{
	delete iEngine;
	}

// We don't have any sub-controls
TInt CSparkControl::CountComponentControls() const
	{
	return 0;
	}

// Empty implementation
void CSparkControl::HandleControlEventL(CCoeControl* /*aControl*/, TCoeEvent /*aEventType*/)
    {
    }

// Handle resource change event, here it is the event that occurs
// when screen mode changes.
void CSparkControl::HandleResourceChange( TInt aType )
	{
	CCoeControl::HandleResourceChange( aType );
	if ( aType == KEikDynamicLayoutVariantSwitch )
		{
		SetExtentToWholeScreen();
		}
	}

void CSparkControl::SizeChanged()
	{
	//	Notify the toolkit
	}

// This event handler detects key events and calls the engine accordingly
TKeyResponse CSparkControl::OfferKeyEventL( const TKeyEvent &aKeyEvent,
	TEventCode aType )
	{
	TKeyResponse response = EKeyWasNotConsumed;

	if ( aType == EEventKey )
		{
		switch ( aKeyEvent.iScanCode )
			{
			case EStdKeyEnter:
			case EStdKeyDevice3:
				iEngine->ForceRedraw();
				response = EKeyWasConsumed;
				break;
			
			case EStdKeyHash:
				iEngine->OpenSettingsL();
				response = EKeyWasConsumed;
				break;

			case EStdKeyUpArrow:
				iEngine->NextFunction();
				response = EKeyWasConsumed;
				break;

			case EStdKeyDownArrow:
				iEngine->PreviousFunction();
				response = EKeyWasConsumed;
				break;

			case EStdKeyLeftArrow:
				iEngine->PreviousFractal();
				response = EKeyWasConsumed;
				break;

			case EStdKeyRightArrow:
				iEngine->NextFractal();
				response = EKeyWasConsumed;
				break;
			default:
				break;
			}
		}

	return response;
	}

void CSparkControl::HandlePointerEventL( const TPointerEvent& aPointerEvent )
	{
	HandlePointerEventL( aPointerEvent );
	}
