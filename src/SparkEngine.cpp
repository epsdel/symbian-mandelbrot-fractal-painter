#include "SparkEngine.h"
#include "Mandelbrot.h"
#include "Koch.h"
#include "Sierpinsky.h"
#include "Functions.h"
#include "SparkDocument.h"
#include "Spark.hrh"

#include <eikenv.h>
#include <e32math.h>
#include <aknviewappui.h>

CSparkEngine::CSparkEngine( CEikonEnv& aEnv, CAknViewAppUi* aAppUi) :
	iEnv( aEnv ),
	iAppUi( aAppUi )
	{
	iGc = NULL;
	iRegion = NULL;
	}

CSparkEngine::~CSparkEngine()
	{
	if ( iAnimationTimer )
		{
		iAnimationTimer->Cancel();
		delete iAnimationTimer;
		}

	if ( iDirectScreenAccess )
		{
		iDirectScreenAccess->Cancel();
		delete iDirectScreenAccess;
		}

	delete iBackBuffer;
	iBackBuffer = NULL;

	delete iBackDevice;
	iBackDevice = NULL;

	delete iBackContext;
	iBackContext = NULL;
	
	delete iFractal;
	iFractal = NULL;
	}

void CSparkEngine::ConstructL( RWindow& aWindow, CWsScreenDevice* aScreenDevice )
	{
	iBackBuffer = NULL;
	iBackDevice = NULL;
	iBackContext = NULL;

	iScreenDevice = aScreenDevice;

	// Create Direct Screen Access
	iDirectScreenAccess = CDirectScreenAccess::NewL( iEnv.WsSession(), *aScreenDevice, aWindow, *this );

	// Create back buffer
	iBackBuffer = new ( ELeave ) CFbsBitmap;

	// We always use 16-bit back buffer so that we don't need to
	// worry about changing color-depth when we use custom drawing routines.
	// Instead, we use BitBlt to copy the back buffer onto the display
	// because it handles the color-depth conversion for us, even
	// if BitBlt is a bit slow.
	User::LeaveIfError( iBackBuffer->Create( aScreenDevice->SizeInPixels(), SPARKENGINE_OFFSCREEN_DISPLAYMODE) );

	RDebug::Print(_L("DisplayMode: %d, off-screen mode: %d"), aScreenDevice->DisplayMode(), SPARKENGINE_OFFSCREEN_DISPLAYMODE);

	// Create Bitmap Context for off-screen bitmap drawing
	iBackDevice = CFbsBitmapDevice::NewL( iBackBuffer );
	User::LeaveIfError( iBackDevice->CreateBitmapContext( iBackContext ) );

	//	setup toolkit
	iToolKit.SetScreenView( aScreenDevice->SizeInPixels() );
	iToolKit.SetFitRatios( ETrue );
	iToolKit.SetScaleWidth( EFalse ); //	scale height when matching aspect ratios
//	iToolKit.SetLandscape( ETrue );
//	iToolKit.SetMirrored( ETrue );

	iIndex = 1;
	iReconstruct = ETrue;
	
	// Start direct screen access
	iDirectScreenAccess->StartL();
	}

TInt CSparkEngine::Update( TAny* aObj )
	{
	static_cast<CSparkEngine*> ( aObj )->Render();
	return 1;
	}

void CSparkEngine::Restart( RDirectScreenAccess::TTerminationReasons /* aReason */)
	{
	// Initialise DSA
	TRAPD(dsaErr, iDirectScreenAccess->StartL());
	if ( dsaErr == KErrNone )
		{
		iGc = iDirectScreenAccess->Gc();
		iRegion = iDirectScreenAccess->DrawingRegion();
		iGc->SetClippingRegion( iRegion );

		// Start animation timer
		if ( !iAnimationTimer )
			{
			TRAPD (error, iAnimationTimer = CPeriodic::NewL( CActive::EPriorityLow ));
			if ( KErrNone != error )
				{
				//	busted
				}
			}
		if ( !iAnimationTimer->IsActive() )
			{
			iAnimationTimer->Start( SPARKENGINE_UPDATE_DELAY, SPARKENGINE_UPDATE_DELAY, TCallBack( &CSparkEngine::Update, this ) );
			}
		}
	}

void CSparkEngine::AbortNow( RDirectScreenAccess::TTerminationReasons /* aReason */)
	{
	if ( iAnimationTimer )
		{
		iAnimationTimer->Cancel();
		delete iAnimationTimer;
		iAnimationTimer = NULL;
		}
	}

void CSparkEngine::SetPaused( TBool aPause )
	{
	iPaused = aPause;
	if ( iPaused )
		iFractal->Suspend();
	else
		iFractal->Restore();
	}

TBool CSparkEngine::IsPaused() const
	{
	return iPaused;
	}

void CSparkEngine::PreviousFractal ()
	{
	if ( iIndex - 1 >= 0 )
		{
		--iIndex;
		iReconstruct = ETrue;
		}	
	}

void CSparkEngine::NextFractal()
	{
	if ( iIndex + 1 < SPARKENGINE_FRACTAL_COUNT )
		{
		++iIndex;
		iReconstruct = ETrue;
		}
	}

void CSparkEngine::NextFunction()
	{
	CMandelbrot* mandel = dynamic_cast<CMandelbrot*> ( iFractal );
	if ( mandel )
		{
		TBool changed = ETrue;
		if ( mandel->GetFunction() == &ZSquarePlusC )
			{
			mandel->SetFunction( &ZCubePlusC );
			}
		else if ( mandel->GetFunction() == &ZCubePlusC )
			{
			mandel->SetFunction( &ZQuadPlusC );
			}
		else if ( mandel->GetFunction() == &ZQuadPlusC )
			{
			mandel->SetFunction( &ZHexaPlusC);
			}
		else if ( mandel->GetFunction() == &ZHexaPlusC )
			{
			mandel->SetFunction( &Custom );
			}
		else if ( mandel->GetFunction() == &Custom )
			{
			mandel->SetFunction( &ZSquarePlusC );
			}
		else
			{
			changed = EFalse;
			}
		if ( changed )
			{
			mandel->Invalidate();
			}
		}
	}

void CSparkEngine::PreviousFunction()
	{
	CMandelbrot* mandel = dynamic_cast<CMandelbrot*> ( iFractal );
	if ( mandel )
		{
		TBool changed = ETrue;
		if ( mandel->GetFunction() == &ZSquarePlusC )
			{
			mandel->SetFunction( &Custom );
			}
		else if ( mandel->GetFunction() == &ZCubePlusC )
			{
			mandel->SetFunction( &ZSquarePlusC );
			}
		else if ( mandel->GetFunction() == &ZQuadPlusC )
			{
			mandel->SetFunction( &ZCubePlusC );
			}
		else if ( mandel->GetFunction() == &ZHexaPlusC )
			{
			mandel->SetFunction( &ZQuadPlusC );
			}
		else if ( mandel->GetFunction() == &Custom )
			{
			mandel->SetFunction( &ZHexaPlusC );
			}
		else
			{
			changed = EFalse;
			}
		if ( changed )
			{
			mandel->Invalidate();
			}
		}
	}

void CSparkEngine::OpenSettingsL()
	{
	if ( iAppUi )
		{
		iAppUi->HandleCommandL( ECommandOpenSettingsView );
		}
	}

void CSparkEngine::ForceRedraw()
	{
	if ( iFractal )
		{
		iFractal->Invalidate();
		}
	}

void CSparkEngine::Render()
	{
	if ( iReconstruct )
		{
		TRAP_IGNORE (ReconstructFractalL());
		}
	
	if (NULL == iFractal)
		return;

	//	cleanup if must
	if ( CFractalBase::EInvalid == iFractal->GetState() )
		{
		iBackContext->SetBrushColor( KRgbBlack );
		iBackContext->Clear();
		}	

	if (CFractalBase::EComplete != iFractal->GetState())
		{
		iFractal->Render( *iBackContext );

		// Copy backbuffer to screen
		// BitBlt performs color-depth conversion so we don't need to worry about it.
		iGc->BitBlt( TPoint( 0, 0 ), iBackBuffer );

		iDirectScreenAccess->ScreenDevice()->Update();
		}	
	}

void CSparkEngine::ReconstructFractalL()
	{
	if ( iFractal != NULL )
		{
		delete iFractal;
		iFractal = NULL;
		}

	switch ( iIndex )
		{
		case 0:
			{
			TFileName name;
			iEnv.FsSession().PrivatePath( name );
			name.Append( _L ("red.txt") );
			
			CMandelbrot* ptr = CMandelbrot::NewL( iToolKit, name );
			iFractal = ptr;
			iToolKit.SetLandscape( ETrue );
			iToolKit.SetWorldView( TComplex( -2, 1.5 ), 4, 3 );
			}
			break;

		case 1:
			{
			CKoch* ptr = CKoch::NewL( iToolKit );
			ptr->SetCenter( TComplex( 1, 1 ) );
			ptr->SetDistanceFromCenter( 1 );
			iFractal = ptr;
			iToolKit.SetLandscape( EFalse );
			iToolKit.SetWorldView( TComplex( 0, 2 ), 2, 2 );			
			break;
			}

		case 2:
			{
			CSierpinsky* ptr = CSierpinsky::NewL( iToolKit );
			iFractal = ptr;
			iToolKit.SetLandscape( EFalse );
			iToolKit.SetWorldView( TComplex( -2, 2 ), 4, 4 );			
			break;
			}
			
		default:
			break;
		}

	if ( NULL != iFractal )
		{
		CSparkDocument::Static()->SetActiveFractal( iFractal );
		iReconstruct = EFalse;
		iFractal->Invalidate();
		}
	}
