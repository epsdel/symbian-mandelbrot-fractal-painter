#include "Mandelbrot.h"
#include "Functions.h"
#include "FractalVisitor.h"

typedef TBuf8<256> TCharBuffer;

CMandelbrot* CMandelbrot::NewLC( const TToolkit& aToolkit, const TFileName& aPaletteFileName )
	{
	CMandelbrot* self = new ( ELeave ) CMandelbrot( aToolkit );
	CleanupStack::PushL( self );
	self->ConstructL( aPaletteFileName );
	return self;
	}

CMandelbrot* CMandelbrot::NewL( const TToolkit& aToolkit, const TFileName& aPaletteFileName )
	{
	CMandelbrot* self = CMandelbrot::NewLC( aToolkit, aPaletteFileName );
	CleanupStack::Pop( self );
	return self;
	}

CMandelbrot::CMandelbrot( const TToolkit& aToolkit ) :
	CFractalBase( aToolkit )
	{
	}

CMandelbrot::~CMandelbrot()
	{
	iPalette.Close();
	}

void CMandelbrot::ConstructL( const TFileName& aFileName )
	{
	iFunction = &ZSquarePlusC;
	iSeed.SetXY( 0, 0 );
	iBailoutRadius = 4;
	iMaxIterations = 128;
	iPixelsToPlotCount = 0;
	iPlottedPixelsCount = 0;

	if ( !LoadPaletteFromFileL( aFileName, iPalette ) )
		{
		for ( TInt idx = 0; idx < 256; ++idx )
			{
			iPalette.AppendL( TRgb( idx, idx, idx ) );
			}
		}
	}

void CMandelbrot::RenderTask( CGraphicsContext& aContext )
	{
	TInt kdx = 0; //	current iteration
	TReal absSqr = 0;

	TUint pixelsToPlotInCurrentChunk = iToolkit.GetScreenView().Height() * FRACTAL_LINE_COUNT_PER_TASK;
	TUint plottedPixels = 0;

	TComplex z;
	TComplex result;

	while ( !iIterator.IsDone() && plottedPixels <= pixelsToPlotInCurrentChunk )
		{
		z = iSeed;
		kdx = 0;
		do
			{
			//	make a copy of the cursor, since the iterator returns a const reference
			result = iIterator.GetCursor();

			//	apply screen to world transform
			ApplyTransform( iToolkit.GetScreenToWorld(), result );

			z = ( *iFunction )( z, result );

			absSqr = z.iX * z.iX + z.iY * z.iY;

			//	absoluteValue = z.Abs();
			//	sqrt (z.x * z.x + z.y * z.y) <= iBailoutRadius | ^2
			//	z.x * z.x + z.y * z.y <= iBailoutRadius ^2 

			++kdx;
			}
		while ( absSqr <= iBailoutRadius * iBailoutRadius && kdx <= iMaxIterations );

		aContext.SetPenColor( iPalette[kdx % 256] );
		aContext.Plot( ConvertToPoint( iIterator.GetCursor() ) );

		++plottedPixels;

		iIterator.Step();
		}

	iPlottedPixelsCount += pixelsToPlotInCurrentChunk;
	}

void CMandelbrot::Render( CGraphicsContext& aContext )
	{
	switch ( iState )
		{
		case ENextTask:
			//	we want the light on while we do rendering
			User::ResetInactivityTime();

			RenderTask( aContext );

			if ( iPlottedPixelsCount > iPixelsToPlotCount )
				{
				iState = EComplete;
				}
			else
				{
				iState = ENextTask;
				}
			break;

		case ESuspended:
			break;

		case EComplete:
			//	do nothing, keep the image
			break;

		case EInvalid:
		default:
			{
			//	reset the iterator
			iIterator.Reset( 
				ConvertToComplex( iToolkit.GetScreenView().iTl ),
				ConvertToComplex( iToolkit.GetScreenView().iBr ) 
			);

			//	update pixel rendering progress info
			iPixelsToPlotCount = iToolkit.GetScreenView().Width()
				* iToolkit.GetScreenView().Height();
			iPlottedPixelsCount = 0;

			iState = ENextTask;
			break;
			}
		}
	}

void CMandelbrot::SetSeed( TComplex aSeed )
	{
	iSeed = aSeed;
	}

TComplex CMandelbrot::GetSeed() const
	{
	return iSeed;
	}

void CMandelbrot::SetBailoutRadius( TReal aRadius )
	{
	iBailoutRadius = aRadius;
	}

TReal CMandelbrot::GetBailoutRaidius() const
	{
	return iBailoutRadius;
	}

void CMandelbrot::SetMaxIterations( TInt aMax )
	{
	iMaxIterations = aMax;
	}

TInt CMandelbrot::GetMaxIterations() const
	{
	return iMaxIterations;
	}

void CMandelbrot::SetFunction( TIterativeFunction aFunction )
	{
	iFunction = aFunction;
	}

TIterativeFunction CMandelbrot::GetFunction() const
	{
	return iFunction;
	}

TPalette& CMandelbrot::GetPalette()
	{
	return iPalette;
	}

TBool CMandelbrot::LoadPaletteFromFileL( const TDesC& aPath, TPalette& aPalette )
	{
	//	connect to a session
	RFs fs;
	User::LeaveIfError( fs.Connect() );
	CleanupClosePushL( fs );
	
	//	open the file
	RFile file;
	TInt err = file.Open( fs, aPath, EFileShareExclusive | EFileRead );
	if ( KErrNone != err )
		{
		CleanupStack::PopAndDestroy( 1, &fs );
		return EFalse;
		}
	CleanupClosePushL( file );

	TInt value = 0; //	one color value for red, green or blue
	TBool collecting = EFalse; //	collecting digits
	TBool added[3] =
		{
		EFalse, EFalse, EFalse
		}; //	added red, green or blue?	
	TRgb color; //	rgb values

	//	read from file
	TCharBuffer buffer;
	TInt length = 0;
	do
		{
		err = file.Read( buffer );
		if (KErrNone != err)
			{
			CleanupStack::PopAndDestroy( 2, &fs );
			return EFalse;
			}

		length = buffer.Length();

		if ( length > 0 )
			{
			for ( TInt idx = 0; idx < length; ++idx )
				{
				if ( IsDigit( buffer[idx] ) )
					{
					if ( collecting )
						{
						TInt8 digit = buffer[idx] - '0';
						value *= 10;
						value += digit;
						}
					else
						{
						collecting = ETrue;
						value = buffer[idx] - '0';
						}
					}
				else
					{ //	any other character is considered a separator
					if ( collecting )
						{
						collecting = EFalse; //	done collecting digits

						if ( added[0] == EFalse )
							{
							color.SetRed( value );
							added[0] = ETrue;
							added[1] = EFalse;
							added[2] = EFalse;
							}
						else if ( added[1] == EFalse )
							{
							color.SetGreen( value );
							//	added[0] is already true
							added[1] = ETrue;
							added[2] = EFalse;
							}
						else if ( added[2] == EFalse )
							{
							color.SetBlue( value );

							aPalette.Append( color );

							//	reset
							added[0] = EFalse;
							added[1] = EFalse;
							added[2] = EFalse;
							
							color.SetRed( 0 );
							color.SetGreen( 0 );
							color.SetBlue( 0 );
							}
						}
					}
				}
			}
		}
	while ( length > 0 );

	//	cleanup
	file.Close();
	CleanupStack::PopAndDestroy( 2, &fs );
	
	return ETrue;
	}

void CMandelbrot::AcceptVisitorL( MFractalVisitor& aVisitor ) const
	{
	aVisitor.VisitL( *this );
	}
