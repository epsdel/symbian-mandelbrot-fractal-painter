/*
 ============================================================================
 Name		: SparkDocument.cpp
 Author	  : Dan M
 Copyright   : Use at your own risk
 Description : CSparkDocument implementation
 ============================================================================
 */

// INCLUDE FILES
#include "SparkAppUi.h"
#include "SparkDocument.h"

#include "Mandelbrot.h"
#include "Koch.h"
#include "Sierpinsky.h"

// ============================ MEMBER FUNCTIONS ===============================

// -----------------------------------------------------------------------------
// CSparkDocument::NewL()
// Two-phased constructor.
// -----------------------------------------------------------------------------
//
CSparkDocument* CSparkDocument::NewL( CEikApplication& aApp )
	{
	CSparkDocument* self = NewLC( aApp );
	CleanupStack::Pop( self );
	return self;
	}

// -----------------------------------------------------------------------------
// CSparkDocument::NewLC()
// Two-phased constructor.
// -----------------------------------------------------------------------------
//
CSparkDocument* CSparkDocument::NewLC( CEikApplication& aApp )
	{
	CSparkDocument* self = new ( ELeave ) CSparkDocument( aApp );

	CleanupStack::PushL( self );
	self->ConstructL();
	return self;
	}

// -----------------------------------------------------------------------------
// CSparkDocument::ConstructL()
// Symbian 2nd phase constructor can leave.
// -----------------------------------------------------------------------------
//
void CSparkDocument::ConstructL()
	{
	}

// -----------------------------------------------------------------------------
// CSparkDocument::CSparkDocument()
// C++ default constructor can NOT contain any code, that might leave.
// -----------------------------------------------------------------------------
//
CSparkDocument::CSparkDocument( CEikApplication& aApp ) :
	CAknDocument( aApp )
	{
	// No implementation required
	}

// ---------------------------------------------------------------------------
// CSparkDocument::~CSparkDocument()
// Destructor.
// ---------------------------------------------------------------------------
//
CSparkDocument::~CSparkDocument()
	{
	}

CSparkDocument* CSparkDocument::Static()
	{
	CAknAppUi* appUi = iAvkonAppUi;
	if ( appUi )
		{
		CSparkDocument* temp = REINTERPRET_CAST(CSparkDocument*, iAvkonAppUi->Document());
		return temp;
		}
	return NULL;
	}

void CSparkDocument::SetActiveFractal (CFractalBase* aFractal)
	{
	iFractal = aFractal;
	}

CFractalBase* CSparkDocument::GetActiveFractal() const
	{
	return iFractal;
	}

// ---------------------------------------------------------------------------
// CSparkDocument::CreateAppUiL()
// Constructs CreateAppUi.
// ---------------------------------------------------------------------------
//
CEikAppUi* CSparkDocument::CreateAppUiL()
	{
	// Create the application user interface, and return a pointer to it;
	// the framework takes ownership of this object
	return new ( ELeave ) CSparkAppUi;
	}

// End of File
