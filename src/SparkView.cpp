/*
 * WheelView.cpp
 *
 *  Created on: Jul 10, 2009
 *      Author: dev
 */

#include "SparkView.h"
#include "SparkControl.h"
#include <aknviewappui.h> 

// Static constructor
CSparkView* CSparkView::NewLC()
	{
	CSparkView* self = new ( ELeave ) CSparkView;
	CleanupStack::PushL( self );
	self->ConstructL();
	return self;
	}

CSparkView* CSparkView::NewL()
	{
	CSparkView* self = NewLC();
	CleanupStack::Pop( self );
	return self;
	}

// Default constructor
CSparkView::CSparkView()
    {
    iContainer = NULL;
    }

// Second-phase constructor
void CSparkView::ConstructL()
	{
	BaseConstructL();
	}

// Destructor
CSparkView::~CSparkView()
	{
	DoDeactivate();
	}

// Destroy control when view is deactivated.
void CSparkView::DoDeactivate()
	{
	if ( iContainer )
		{
		AppUi()->RemoveFromStack( iContainer );
		delete iContainer;
		iContainer = NULL;
		}
	}

void CSparkView::DoActivateL( const TVwsViewId& /*aPrevViewId*/, TUid /*aCustomMessageId*/, const TDesC8& /*aCustomMessage*/)
	{
	iContainer = new ( ELeave ) CSparkControl();
	iContainer->SetMopParent( this );
	iContainer->ConstructL( AppUi()->ClientRect(), AppUi() ); // For full screen
	AppUi()->AddToStackL( *this, iContainer );
	}

TUid CSparkView::Id() const
	{
	return KSparkViewId;
	}

// Handle command
void CSparkView::HandleCommandL( TInt aCommand )
	{
	}
