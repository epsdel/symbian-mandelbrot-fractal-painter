/*
 * CWheelAppUi.cpp
 *
 *  Created on: Jul 10, 2009
 *      Author: dev
 */

#include "SparkAppUi.h"
#include "SparkView.h"
#include "SettingsView.h"
#include "Spark.hrh"

// The second-phase constructor of the application UI class.
// The application UI creates and owns the one and only view.
void CSparkAppUi::ConstructL()
	{
	BaseConstructL( EAknEnableSkin );
	// Because the MSK flag cannot currently be retrieved (due to
	// CAknAppUiBase::IsMSKEnabledApp() being undefined, see
	// ScalableScreenDrawingEngine.h for more information), there's no reason
	// for passing the flag to BaseConstructL
	//    BaseConstructL(EAknEnableSkin | EAknEnableMSK);

	iEikonEnv->RootWin().EnableScreenChangeEvents();
	
	CreateViewsL ();	
	}

// Destructor
CSparkAppUi::~CSparkAppUi()
	{
	}

// Called by the UI framework when a command has been issued.
void CSparkAppUi::HandleCommandL( TInt aCommand )
	{
	switch ( aCommand )
		{
		case EAknSoftkeySelect:
			{
			iView->HandleCommandL( EAknSoftkeySelect );
			break;
			}
			
		case ECommandOpenSettingsView:
			SwitchToSettingsViewL();
			break;
			
		case ECommandOpenMainView:
			SwitchToSparkViewL();
			break;
			
		case EAknCmdExit:
		case EEikCmdExit:
		case EAknSoftkeyExit:
			Exit();
			break;
		}
	}

void CSparkAppUi::SwitchToSettingsViewL ()
	{
	ActivateLocalViewL( KSparkSettingsViewId );
	}

void CSparkAppUi::SwitchToSparkViewL ()
	{
	ActivateLocalViewL( KSparkViewId );
	}

void CSparkAppUi::CreateViewsL ()
	{
	iSparkView = CSparkView::NewL();
	AddViewL( iSparkView );

	iSettingsView = CSettingsView::NewL();
	AddViewL( iSettingsView );

	SetDefaultViewL( *iSparkView );

	ActivateLocalViewL( KSparkViewId );
	}
