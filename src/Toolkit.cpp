/*
 * DTK.cpp
 *
 *  Created on: Jul 13, 2009
 *      Author: dev
 */

#include "Toolkit.h"
#include <e32def.h>

TToolkit::TToolkit() :
	iFitRatios( EFalse ), 
	iScaleWidth( ETrue ), 
	iLandscape( EFalse ),
	iMirrored( EFalse )
	{
	}

void TToolkit::SetScreenView(const TRect& aRect)
	{
	iScreenView = aRect;

	if ( iFitRatios )
		{
		FitRatios();
		}
	
	UpdateTransforms();
	}

void TToolkit::SetWorldView(TComplex aTopLeft, TReal aWidth, TReal aHeight)
	{
	iWorldView = TWindow( aTopLeft, TComplex( aTopLeft.iX + aWidth, aTopLeft.iY + aHeight ) );
	
	if ( iFitRatios )
		{
		FitRatios();
		}

	UpdateTransforms();
	}

void TToolkit::SetWorldView(TComplex aTopLeft, TComplex aBottomRight)
	{
	iWorldView = TWindow(aTopLeft, aBottomRight);
	
	if (iFitRatios)
		{
		FitRatios();
		}

	UpdateTransforms();
	}

const TRect& TToolkit::GetScreenView() const
	{
	return iScreenView;
	}

const TWindow& TToolkit::GetWorldView() const
	{
	return iWorldView;
	}

void TToolkit::SetFitRatios(TBool aFitRatios)
	{
	iFitRatios = aFitRatios;
	}

TBool TToolkit::IsFitRatios() const
	{
	return iFitRatios;
	}

void TToolkit::SetScaleWidth(TBool aScaleWidth)
	{
	iScaleWidth = aScaleWidth;
	}

TBool TToolkit::IsScaleWidth() const
	{
	return iScaleWidth;
	}

void TToolkit::SetLandscape( TBool aLandscape )
	{
	iLandscape = aLandscape;
	}

TBool TToolkit::IsLandscape() const
	{
	return iLandscape;
	}

void TToolkit::SetMirrored( TBool aMirrored )
	{
	iMirrored = aMirrored;
	}

TBool TToolkit::IsMirrored() const
	{
	return iMirrored;
	}

const TMatrix2D& TToolkit::GetWorldToScreen() const
	{
	return iWorldToScreen;
	}

const TMatrix2D& TToolkit::GetScreenToWorld() const
	{
	return iScreenToWorld;
	}

void TToolkit::FitRatios()
	{
	TReal worldWidth = static_cast<TReal> (iWorldView.iSecond.iX - iWorldView.iFirst.iX);
	TReal worldHeight = static_cast<TReal> (iWorldView.iSecond.iY - iWorldView.iFirst.iY);
	
	if (iScreenView.Width() != 0 && iScreenView.Height() != 0 && worldWidth != 0 && worldHeight != 0)
		{
		//	compare aspect ratios
		TReal screenRatio = static_cast<TReal> (iScreenView.Width()) / iScreenView.Height();
		TReal worldRatio = worldWidth / worldHeight;

		//	scale width or height
		if (screenRatio != worldRatio)
			{
			if (iScaleWidth)
				{
				//	what is the desired world width?
				TReal desiredWorldWidth = screenRatio * worldHeight;
				TReal halfDesiredWorldWidth = (desiredWorldWidth - worldWidth) / 2;
				iWorldView.iFirst.iX -= halfDesiredWorldWidth;
				iWorldView.iSecond.iX += halfDesiredWorldWidth;
				}
			else
				{
				//	what is the desired world height?
				TReal desiredWorldHeight = (1.0 / screenRatio) * worldWidth;
				TReal halfDesiredWorldHeight = (desiredWorldHeight - worldHeight) / 2;
				iWorldView.iFirst.iY -= halfDesiredWorldHeight;
				iWorldView.iSecond.iY += halfDesiredWorldHeight;
				}
			}
		}
	}

void TToolkit::UpdateTransforms()
	{
	TReal worldWidth = static_cast<TReal> (iWorldView.iSecond.iX - iWorldView.iFirst.iX);
	TReal worldHeight = static_cast<TReal> (iWorldView.iSecond.iY - iWorldView.iFirst.iY);

	if (iScreenView.Width() == 0 || iScreenView.Height() == 0 || worldWidth == 0 || worldHeight == 0)
		{
		//	bye
		LoadIdentity( iScreenToWorld );
		return;
		}

	//	We are going to compute the screen to world transformation matrix into iScreenToWorld
	//	This will be the result of the following multiplication
	//		Rotation * Scale * Translation(-world center) * Mirror * Translation(-screen center) 
	//	For specific scale, mirror and translation values, check the actuall call locations
	//	For the reason why we apply operations in this order, check some good OpenGL documentation
	//	on the subject of the order of applying matrix transformations on primitives 
	
	//	Compute the rotation angle depending on landscape and mirror flags
	TInt angle = 0;
	if ( iLandscape )
		{
		angle += 90;
		}
	if ( iMirrored )
		{
		angle += 180;
		}
	if ( angle != 0 )
		{
		LoadRotation( iScreenToWorld, angle );
		}
	else
		{
		LoadIdentity( iScreenToWorld );
		}
	
	TMatrix2D result; //	this will hold intermediary results
		
	//	apply scale to fit ratios
	TReal widthRatio = worldWidth / iScreenView.Width();
	TReal heightRatio = worldHeight / iScreenView.Height();
	LoadScale( result, widthRatio, heightRatio );
	iScreenToWorld *= result;

	//	the screen center might not correspond to the world center, so perform the required translation
	TComplex worldCenter ( (iWorldView.iSecond.iX - iWorldView.iFirst.iX) / 2, (iWorldView.iSecond.iY - iWorldView.iFirst.iY) / 2);
	if ( worldCenter.iX != 0 && worldCenter.iY != 0 )
		{
		LoadTranslation( result, -worldCenter.iX, -worldCenter.iY );
		iScreenToWorld *= result;
		}
	
	//	apply mirror because in screen coordinates the y axis is inverted
	LoadMirror( result, 'y' );
	iScreenToWorld *= result;

	//	compute screen center and apply translation
	TPoint screenCenter( ( iScreenView.iBr.iX - iScreenView.iTl.iX ) / 2, ( iScreenView.iBr.iY - iScreenView.iTl.iY ) / 2 );
	LoadTranslation( result, -screenCenter.iX, -screenCenter.iY );
	iScreenToWorld *= result;

	ComputeInverse( iScreenToWorld, iWorldToScreen );
	}
