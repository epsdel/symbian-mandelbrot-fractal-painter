/*
 * IterativeFunctions.cpp
 *
 *  Created on: Aug 7, 2009
 *      Author: dev
 */

#include "Functions.h"
#include <e32math.h>

TComplex ZSquarePlusC( const TComplex& aZ, const TComplex& aC )
	{
	TComplex result = aZ;
	result *= result;
	result += aC;
	return result;
	}

TComplex ZCubePlusC( const TComplex& aZ, const TComplex& aC )
	{
	TComplex result = aZ;
	result *= aZ;
	result *= aZ;
	result += aC;
	return result;
	}

TComplex ZQuadPlusC( const TComplex& aZ, const TComplex& aC )
	{
	TComplex result = aZ;
	result *= aZ;
	result *= result;
	result += aC;
	return result;
	}

TComplex ZHexaPlusC( const TComplex& aZ, const TComplex& aC )
	{
	TComplex result = aZ;
	result *= aZ;
	result *= aZ;
	result *= result;
	result += aC;
	return result;
	}

TComplex Custom ( const TComplex& aZ, const TComplex& aC )
	{
	TComplex result = aZ;
	result *= 2;
	result -= aC ;
	return result;
	}
