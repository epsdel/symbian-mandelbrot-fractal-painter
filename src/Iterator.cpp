/*
 * PixelIterator.cpp
 *
 *  Created on: Jul 16, 2009
 *      Author: Dan
 */

#include "Iterator.h"

TIterator::TIterator()
	{
	//	iCursor, iStart, iEnd are all initialized to the same default values, so IsDone () returns ETrue
	}

TIterator::TIterator(const TComplex& aStart, const TComplex& aEnd) :
	iStart (aStart.iX < aEnd.iX ? aStart.iX : aEnd.iX, aStart.iY < aEnd.iY ? aStart.iY : aEnd.iY),
	iEnd (aStart.iX < aEnd.iX ? aEnd.iX : aStart.iX, aStart.iY < aEnd.iY ? aEnd.iY : aStart.iY)
	{
	iCursor = iStart;
	}

void TIterator::Reset()
	{
	iCursor = iStart;
	}

void TIterator::Reset (const TComplex& aStart, const TComplex& aEnd)
	{
	iStart.iX = ( aStart.iX < aEnd.iX ) ? aStart.iX : aEnd.iX;
	iStart.iY = ( aStart.iY < aEnd.iY ) ? aStart.iY : aEnd.iY;
	iEnd.iX = ( aStart.iX < aEnd.iX ) ? aEnd.iX : aStart.iX;
	iEnd.iY = ( aStart.iY < aEnd.iY ) ? aEnd.iY : aStart.iY;
	iCursor = iStart;
	}

void TIterator::Step()
	{
	//	advance cursor towards end
	
	if ( iCursor != iEnd )
		{
		if (iCursor.iY < iEnd.iY)
			{
			++iCursor.iY;
			}
		else
			{
			if (iCursor.iX < iEnd.iX)
				{
				iCursor.iY = iStart.iY;
				++iCursor.iX;
				}
			}
		}	
	}

TBool TIterator::IsDone() const
	{
	return (iCursor == iEnd);
	}

const TComplex& TIterator::GetCursor() const
	{
	return iCursor;
	}
