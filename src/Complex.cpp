/*
 * Complex.cpp
 *
 *  Created on: Apr 10, 2009
 *      Author: dev
 */

#include "Complex.h"
#include "Matrix2D.h"
#include <e32math.h>

TComplex::TComplex() :
	iX( 0 ), iY( 0 )
	{
	}

TComplex::TComplex( TReal aX, TReal aY ) :
	iX( aX ), iY( aY )
	{
	}

void TComplex::SetXY( TReal aX, TReal aY )
	{
	iX = aX;
	iY = aY;
	}

TComplex& TComplex::operator*=( const TComplex& aComplex )
	{
	TReal x = iX * aComplex.iX - iY * aComplex.iY;
	TReal y = iX * aComplex.iY + aComplex.iX * iY;

	iX = x;
	iY = y;

	return *this;
	}

TComplex& TComplex::operator*=( TReal aScalar )
	{
	iX *= aScalar;
	iY *= aScalar;
	return *this;
	}

TComplex& TComplex::operator+=( const TComplex& aComplex )
	{
	iX += aComplex.iX;
	iY += aComplex.iY;
	return *this;
	}

TComplex& TComplex::operator-=( const TComplex& aComplex )
	{
	iX -= aComplex.iX;
	iY -= aComplex.iY;
	return *this;
	}

TComplex TComplex::operator+( const TComplex& aComplex )
	{
	return TComplex( iX + aComplex.iX, iY + aComplex.iY );
	}

TBool TComplex::operator==( const TComplex& aComplex ) const
	{
	return ( iX == aComplex.iX ) && ( iY == aComplex.iY );
	}

TBool TComplex::operator !=( const TComplex& aComplex ) const
	{
	return ( iX != aComplex.iX ) || ( iY != aComplex.iY );
	}

TReal TComplex::Abs() const
	{
	TReal result;
	Math::Sqrt( result, iX * iX + iY * iY );
	return result;
	}

TComplex TComplex::Sqr() const
	{
	return TComplex( iX * iX - iY * iY, 2 * iX * iY );
	}

TComplex operator* (const TComplex& aComplex, TReal aScalar)
	{
	return TComplex( aComplex.iX * aScalar, aComplex.iY * aScalar );	
	}

TComplex operator* (TReal aScalar, const TComplex& aComplex)
	{
	return operator*( aComplex, aScalar );
	}
