/*
 * Koch.cpp
 *
 *  Created on: Jul 29, 2009
 *      Author: dev
 */

#include "Koch.h"
#include "Toolkit.h"
#include "FractalVisitor.h"

#include <gdi.h>

CKoch* CKoch::NewLC( const TToolkit& aToolkit )
	{
	CKoch* self = new ( ELeave ) CKoch( aToolkit );
	CleanupStack::PushL( self );
	self->ConstructL();
	return self;
	}

CKoch* CKoch::NewL( const TToolkit& aToolkit )
	{
	CKoch* self = CKoch::NewLC( aToolkit );
	CleanupStack::Pop( self );
	return self;
	}

CKoch::CKoch( const TToolkit& aToolkit ) :
	CFractalBase( aToolkit ),
	iMaxRecursionDepth( KOCH_DEFAULT_MAX_RECURSION_DEPTH),
	iDistanceFromCenter( KOCH_DEFAULT_DISTANCE_FROM_CENTER )
	{
	}

CKoch::~CKoch ()
	{
	}

void CKoch::ConstructL()
	{
	iBackColor = KRgbBlack;
	iColor = KRgbBlue;
	}

void CKoch::SetMaxRecursionDepth( TInt aLevel )
	{
	iMaxRecursionDepth = aLevel;
	}

TInt CKoch::GetMaxRecursionDepth() const
	{
	return iMaxRecursionDepth;
	}

void CKoch::SetDistanceFromCenter( TReal aDist )
	{
	iDistanceFromCenter = aDist;
	}

TReal CKoch::GetDistanceFromCenter() const
	{
	return iDistanceFromCenter;
	}

void CKoch::SetColor( TRgb aColor )
	{
	iColor = aColor;
	}

TRgb CKoch::GetColor() const
	{
	return iColor;
	}

void CKoch::SetBackColor( TRgb aColor )
	{
	iBackColor = aColor;
	}

TRgb CKoch::GetBackColor() const
	{
	return iBackColor;
	}

void CKoch::SetCenter( TComplex aCenter )
	{
	iCenter = aCenter;
	}

TComplex CKoch::GetCenter() const
	{
	return iCenter;
	}

void CKoch::Render( CGraphicsContext& aContext )
	{
	switch ( iState )
		{
		case ENextTask:
			{
			RenderSnowflake( 0, aContext, iPoints[0], iPoints[1] );
			RenderSnowflake( 0, aContext, iPoints[2], iPoints[0] );
			RenderSnowflake( 0, aContext, iPoints[1], iPoints[2] );
			iState = EComplete;
			}
			break;

		case ESuspended:
			//	rendering was suspended, do nothing
			break;

		case EComplete:
			//	we're done
			break;

		case EInvalid:
		default:
			{
			//	(re)set to initial state
			ResetPoints();
			iState = ENextTask;
			break;
			}
		}
	}

void CKoch::ResetPoints()
	{
	TMatrix2D result;	//	intermediary transform
	TMatrix2D transform; //	final transform, the one that we'll apply

	LoadRotation( transform, 150 );
	LoadTranslation( result, -iCenter.iX, -iCenter.iY );
	transform *= result;
	iPoints[0].SetXY( iCenter.iX + iDistanceFromCenter, iCenter.iY );
	ApplyTransform( transform, iPoints[0] );

	LoadRotation( transform, 30 );
	LoadTranslation( result, -iCenter.iX, -iCenter.iY );
	transform *= result;
	iPoints[1].SetXY( iCenter.iX + iDistanceFromCenter, iCenter.iY );
	ApplyTransform( transform, iPoints[1] );

	LoadRotation( transform, -90 );
	LoadTranslation( result, -iCenter.iX, -iCenter.iY );
	transform *= result;
	iPoints[2].SetXY( iCenter.iX + iDistanceFromCenter, iCenter.iY );
	ApplyTransform( transform, iPoints[2] );
	}

void CKoch::RenderSnowflake( TInt aDepth, CGraphicsContext& aContext, const TComplex& aPt1, const TComplex& aPt2 )
	{
	if ( aDepth >= iMaxRecursionDepth )
		{
		return;
		}

	TReal32 t1 = 1.0f / 3;
	TReal32 t2 = 2.0f / 3;

	//	inner points
	TComplex pts[3] =
		{
		TComplex( ( 1 - t1 ) * aPt1 + t1 * aPt2 ),
		TComplex( ( 1 - t2 ) * aPt1 + t2 * aPt2 )
		};

	pts[2] = pts[1];

	//	setup and apply transform
	TMatrix2D transform;
	LoadTranslation( transform, pts[0].iX, pts[0].iY );
	TMatrix2D result;
	LoadRotation( result, 60 );
	transform *= result;
	LoadTranslation( result, -pts[0].iX, -pts[0].iY );
	transform *= result;
	ApplyTransform( transform, pts[2] );

	//	draw
	aContext.SetBrushStyle( CGraphicsContext::ENullBrush );
	aContext.SetPenStyle( CGraphicsContext::ESolidPen );
	
	TComplex temp1 = aPt1;
	TComplex temp2 = aPt2;

	//	clear line
	aContext.SetPenColor( iBackColor );
	ApplyTransform( iToolkit.GetWorldToScreen(), temp1 );
	ApplyTransform( iToolkit.GetWorldToScreen(), temp2 );
	aContext.DrawLine( ConvertToPoint( temp1 ), ConvertToPoint( temp2 ) );

	//	draw 2 lines
	aContext.SetPenColor( iColor );
	temp1 = aPt1;
	temp2 = pts[0];
	ApplyTransform( iToolkit.GetWorldToScreen(), temp1 );
	ApplyTransform( iToolkit.GetWorldToScreen(), temp2 );
	aContext.DrawLine( ConvertToPoint( temp1 ), ConvertToPoint( temp2 ) );

	temp1 = pts[1];
	temp2 = aPt2;
	ApplyTransform( iToolkit.GetWorldToScreen(), temp1 );
	ApplyTransform( iToolkit.GetWorldToScreen(), temp2 );
	aContext.DrawLine( ConvertToPoint( temp1 ), ConvertToPoint( temp2 ) );

	//	draw smaller triangle lines
	temp1 = pts[0];
	temp2 = pts[2];
	ApplyTransform( iToolkit.GetWorldToScreen(), temp1 );
	ApplyTransform( iToolkit.GetWorldToScreen(), temp2 );
	aContext.DrawLine( ConvertToPoint( temp1 ), ConvertToPoint( temp2 ) );

	temp1 = pts[1];
	temp2 = pts[2];
	ApplyTransform( iToolkit.GetWorldToScreen(), temp1 );
	ApplyTransform( iToolkit.GetWorldToScreen(), temp2 );
	aContext.DrawLine( ConvertToPoint( temp1 ), ConvertToPoint( temp2 ) );

	RenderSnowflake( aDepth + 1, aContext, aPt1, pts[0] );
	RenderSnowflake( aDepth + 1, aContext, pts[0], pts[2] );
	RenderSnowflake( aDepth + 1, aContext, pts[2], pts[1] );
	RenderSnowflake( aDepth + 1, aContext, pts[1], aPt2 );
	}

void CKoch::AcceptVisitorL( MFractalVisitor& aVisitor ) const
	{
	aVisitor.VisitL( *this );
	}
