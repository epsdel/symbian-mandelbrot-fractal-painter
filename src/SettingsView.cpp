/*
 * SettingsView.cpp
 *
 *  Created on: Sep 30, 2009
 *      Author: dan.munteanu
 */

#include "SettingsView.h"
#include "SettingsControl.h"
#include "Mandelbrot.h"
#include "Koch.h"
#include "Sierpinsky.h"
#include "SparkDocument.h"

#include <aknviewappui.h> 

CSettingsView* CSettingsView::NewL()
	{
	CSettingsView* self = CSettingsView::NewLC();
	self->ConstructL();
	CleanupStack::Pop( self );
	return self;
	}

CSettingsView* CSettingsView::NewLC()
	{
	CSettingsView* self = new ( ELeave ) CSettingsView;
	CleanupStack::PushL( self );
	return self;
	}

CSettingsView::CSettingsView ()
	{
	}

CSettingsView::~CSettingsView ()
	{
	DoDeactivate();
	}

void CSettingsView::ConstructL ()
	{
	BaseConstructL();
	}

TUid CSettingsView::Id() const
	{
	return KSparkSettingsViewId;
	}

void CSettingsView::DoActivateL( const TVwsViewId& /* aPrevViewId */,
	TUid /* aCustomMessageId */, const TDesC8& /* aCustomMessage */ )
	{
	if ( NULL == iControl )
		{
		iControl = new ( ELeave ) CSettingsControl;
		iControl->SetMopParent( this );
		iControl->ConstructL( AppUi()->ClientRect(), AppUi() );
		AppUi()->AddToStackL( *this, iControl );
		
		//	load fractal - specific settings
		CFractalBase* fractal = CSparkDocument::Static()->GetActiveFractal();
		if ( NULL != fractal )
			{
			fractal->AcceptVisitorL( *this );
			}		
		}
	}

void CSettingsView::DoDeactivate()
	{
	if ( iControl )
		{
		AppUi()->RemoveFromStack( iControl );
		delete iControl;
		iControl = NULL;
		}
	}

void CSettingsView::VisitL( const CMandelbrot& aFractal )
	{
	if ( iControl )
		{
		iControl->SetIndex( 0 );
		}
	}

void CSettingsView::VisitL( const CKoch& aFractal )
	{
	if ( iControl )
		{
		iControl->SetIndex( 1 );
		}
	}

void CSettingsView::VisitL( const CSierpinsky& aFractal )
	{
	if ( iControl )
		{
		iControl->SetIndex( 2 );
		}
	}

void CSettingsView::VisitL( const CFractalBase& aFractal )
	{
	if ( iControl )
		{
		iControl->SetIndex( -1 );
		}
	}
